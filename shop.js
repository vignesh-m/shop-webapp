var test_pic = 'shop/pic_trees_forest.jpg'
var test_shop_items = [
    {
        'id': 1,
        'picture': test_pic,
        'title': 'Item 1'    
    }, 
    {
        'id': 2,
        'picture': test_pic,
        'title': 'Item 2'    
    },
    {
        'id': 3,
        'picture': test_pic,
        'title': 'Item 3'    
    },
    {
        'id': 4,
        'picture': test_pic,
        'title': 'Item 4'    
    },
    {
        'id': 5,
        'picture': test_pic,
        'title': 'Item 5'    
    }
];
function items_to_row(shop_items) {
    // given items, split into rows of <=3 each
    var l = shop_items.length;
    var rl = (l + 2) / 3 //ceil(l/3)
    var shop_rows = []
    for (var i = 0; i < rl; i++){
        shop_rows[i] = []
        for (var j = 0; j < 3; j++){
            var idx = 3 * i + j;
            if(idx < l)
                shop_rows[i][j] = shop_items[idx]
        }
    }
    return shop_rows
}

// cart structure - {items: {<item id>: {count: <count>, item: <item>}}}
function addToCart(item, cart) {
    cart = cart.items;
    if (item.id in cart)
        Vue.set(cart, item.id, { count: cart[item.id].count + 1, item: cart[item.id].item });
    else
        Vue.set(cart, item.id, { count: 1, item: item });
}
function removeFromCart(item, cart) {
    cart = cart.items;
    if (item.id in cart) {
        Vue.set(cart, item.id, { count: cart[item.id].count - 1, item: cart[item.id].item });
        if (cart[item.id].count == 0)
            Vue.delete(cart, item.id)
    } else {
        ;
    }
}

Vue.component('shop-item', {
    props: ['item', 'cart'],
    template: ` 
        <div>
            <img v-bind:src="item.picture" class="item-pic" />
            <span class="item-title"> {{item.title}} </span> <br/>
            <span class="item-plus glyphicon glyphicon-plus-sign link-span" v-on:click="add"></span>
            <span class="item-minus glyphicon glyphicon-minus-sign link-span" v-on:click="remove"></span>
            <button v-on:click="test"> Test </button>
        </div>
    `,
    methods: {
        add: function () {
            console.log("adding", this.item.id)
            addToCart(this.item, this.cart);
        },
        remove: function () {
            console.log("removing", this.item.id)
            removeFromCart(this.item, this.cart);
        },
        test: function () {
            console.log('cart', this.cart);
        }
    }
});
Vue.component('cart-item', {
    props: ['item', 'count'],
    template: `
        <div>
            {{item.title}} x{{count}}
        </div>
    `
});
Vue.component('cart-dropdown', {
    props: ['cart'],
    template: `
        <ul class="dropdown-menu" id="cart-dropdown">
            <li v-for="(value, itemId) in cart.items" >
                <cart-item v-bind:item=value.item v-bind:count=value.count> </cart-item>
            </li>
        </ul>
    `
    
})

/*
*/
var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    shop_item_data: test_shop_items,
    shop_row_data: items_to_row(test_shop_items),
    cart: { 'items': {}}
  }
})
